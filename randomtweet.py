import os
import sys
import time

import twitter
from randomtext import randomtext

CHARACTER_LIMIT = 280

def tweet(api):
    randtxt = randomtext.gen_word(4, CHARACTER_LIMIT)
    try:
        api.PostUpdate(randtxt)

        print("Tweeted: " + randtxt)
    except Error as e:
        print("Tweet not successful: " + e)

def loop(api):
    while True:
        last_tweet = api.GetUserTimeline(count=1, trim_user=True) [0]
        last_twt_time = last_tweet.created_at_in_seconds

        if time.time() // 3600 > last_twt_time // 3600: #This only sees if the hour has changed between tweets; It doesn't need to aim for exactly one hour, but it will roughly land within an hour.
            tweet(api)
        else:
            print("Waiting for 5 minutes")

        time.sleep(300) # 5 minute seems like a good amount of time between polling.


def main(*args, **kwargs):
    api = twitter.Api(consumer_key=os.environ.get('randtwt_consumer_key'),
                  consumer_secret=os.environ.get('randtwt_consumer_secret'),
                  access_token_key=os.environ.get('randtwt_access_token_key'),
                  access_token_secret=os.environ.get('randtwt_access_token_secret'))

    if '-t' in args or '--test' in args:
        tweet(api)
        return

    loop(api)

if __name__ == "__main__":
    main(*sys.argv)
